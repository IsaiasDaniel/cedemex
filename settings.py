FILE_NAME = 'CPdescarga.txt'
COAT_OF_ARM = 'escudos'
TRY_GET_LOCATION = 0

MAPPING = {
  "mappings": {
    "estados": {
      "properties": {
        "name": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "location": {
          "type": "geo_point"
        },
        "coatOfArm":{
          "type": "string"
        }
      }
    },
    "municipios": {
      "properties": {
        "name": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "idEstado": {
          "type": "string"
        },
        "estado": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "city": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "location": {
          "type": "geo_point"
        }

      }
    },
    "asentamiento": {
      "properties": {
        "name": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "idEstado": {
          "type": "string"
        },
        "idMunicipio": {
          "type": "string"
        },
        "estado": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "municipio": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "location": {
          "type": "geo_point"
        }
      }
    }
  }
}
