# CEDEMEX  

Herramienta para obtencion de datos del  archivo propocionado por SEPOMEX CVS

CPdescarga.txt


## Requeriments

### BackEnd

- Python
- ElasticSearch
  *
  ```
  { "settings": {
    "analysis": {
      "filter": {
        "spanish_stop": {
          "type":       "stop",
          "stopwords":  "_spanish_"
        },
        "spanish_keywords": {
          "type":       "keyword_marker",
          "keywords":   [""]
        },
        "spanish_stemmer": {
          "type":       "stemmer",
          "language":   "light_spanish"
        }
      },
      "analyzer": {
        "spanish": {
          "tokenizer":  "standard",
          "filter": [
            "lowercase",
            "spanish_stop",
            "spanish_keywords",
            "spanish_stemmer"
          ]
        }
      }
    }
  },
  "mappings": {
    "estados": {
      "properties": {
        "name": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "location": {
          "type": "geo_point"
        },
        "coatOfArm":{
          "type": "string"
        }
      }
    },
    "municipios": {
      "properties": {
        "name": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "idEstado": {
          "type": "string"
        },
        "estado": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "city": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "location": {
          "type": "geo_point"
        }

      }
    },
    "asentamiento": {
      "properties": {
        "name": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "idEstado": {
          "type": "string"
        },
        "idMunicipio": {
          "type": "string"
        },
        "estado": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "municipio": {
          "type": "string",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            },
          "analyzer": "spanish"
        },
        "cp": {
          "type": "string",
          "index": "not_analyzed",
          "analyzer": "spanish"
        },
        "address": {
          "type": "string",
          "analyzer": "spanish",
          "fields": {
              "raw": {
                "type": "string",
                "index": "not_analyzed"
              }
            }
        },
        "location": {
          "type": "geo_point"
        }
      }
    }
  }
}
  ```

## WebComponents

- ReactJS


# TODO
- [ ] crear en automatio el indice de elastic search
- [ ] geolocation con apikey de google
- [ ] componentes web
