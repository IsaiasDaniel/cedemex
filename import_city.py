#! /usr/bin env python
# -*- coding: utf-8 -*-
import settings
import json
import yaml
import base64
import codecs
from geopy.geocoders import Nominatim
from os import path
from elasticsearch import Elasticsearch, helpers

def get_data():
	file_name = getattr(settings, 'FILE_NAME', 'CPdescarga.txt')
	file_path = path.dirname(path.dirname(__file__))
	file_path_full = path.join(file_path, file_name)
	cp = open(file_path_full, 'r')
	return cp

def get_fields(line):
	line = line.decode('utf-8').replace('.', '').split('|')
	return line[4], line[5], line[3], line[1], line[2], line[0]

def get_cord(addres):
	for conect in range(getattr(settings,'TRY_GET_LOCATION', 1)):
		try:
			geolocator = Nominatim()
			location = geolocator.geocode(addres)
			return {'lat': location.latitude,
					'lon':location.longitude}
		except:
			continue
	return {'lat': 0.0,
			'lon': 0.0}

def set_estado(info, estado):
	if estado not in info.keys():
		coat_of_arm_dir = getattr(settings, 'COAT_OF_ARM', 'escudos')
		estado_save = estado.replace(' ','_')
		id = base64.b64encode(estado.encode('utf-8'))
		index = {
			'_municipios':{},
			'_index': 'cedemex',
			'_type': 'estados',
			'_op_type': 'index',
			'_id': id,
			'location': get_cord(estado),
			'name': estado,
			'coatOfArm': u'%s/%s.svg'%(coat_of_arm_dir, estado_save)
		}

		info[estado] = index
	return info

def set_municipio(info, estado_name, municipio, ciudad):
	estado = info[estado_name]['_municipios']
	if municipio not in estado.keys():
		id = base64.b64encode("%s %s"%(
			estado_name.encode('utf-8'), municipio.encode('utf-8')))
		index = {
			'_asentamientos':{},
			'_index': 'cedemex',
			'_type': 'municipios',
			'_op_type': 'index',
			'_id': id,
			'estado': estado_name,
			'name': municipio,
			'location': get_cord("%s %s"%(estado_name,municipio)),
			'ciudad': ciudad
		}
		estado[municipio] = index
	return info

def set_asentamiento(info, estado_name, municipio_name, asentamiento,
		tipo_asentamiento, cp):
	asentamientos = \
		info[estado_name]['_municipios'][municipio_name]['_asentamientos']
	if asentamiento not in asentamientos.keys():
		id = base64.b64encode("%s %s %s"%(
			estado_name.encode('utf-8'),
			municipio_name.encode('utf-8'),
			asentamiento.encode('utf-8')))
		full_name = '%s %s, %s, %s, C.P: %s'%(tipo_asentamiento, asentamiento,
			municipio_name, estado_name, cp)
		index = {
			'id': id,
			'address': full_name,
			'name': asentamiento,
			'estado': estado_name,
			'municipio':municipio_name,
			'location': get_cord("%s %s %s"%(estado_name, municipio_name, asentamiento)),
			'type': tipo_asentamiento,
			'cp': cp
		}
		asentamientos[asentamiento] = index

	return info

def get_data_dict():
	datas =  get_data()
	info = {}
	print "CEDEMEX",
	for data in datas:
		estado, ciudad, municipio, asentamiento,\
		tipo_asentamiento, cp = get_fields(data)
		info = set_estado(info, estado)
		info = set_municipio(info, estado, municipio, ciudad)
		info = set_asentamiento(info, estado, municipio,
			asentamiento, tipo_asentamiento, cp)
	return info


def save(datas):
	es = Elasticsearch()
	estados = []
	for estado_key, estado_value in datas.items():
		print "\n",estado_key
		municipios = []
		for municipio_key, municipio_value in estado_value['_municipios'].items():
			# helpers.bulk(
			# 	es, municipio_value['_asentamientos'].values(), stats_only=True)
			#
			for asentamiento in municipio_value['_asentamientos'].values():
				id_index = asentamiento['id']
				exist = es.exists(index="caradvisor", doc_type="asentamiento", id=id_index)
				if exist:
					es.update(index="cedemex", doc_type="asentamiento",
							  id=id_index, body=json.dumps(asentamiento))
				else:
					es.create(index="cedemex", doc_type="asentamiento",
							  body=json.dumps(asentamiento), id=id_index)
				print '+',

			print '%s.(%s)'%(
				municipio_value['name'], len(municipio_value['_asentamientos']))
			del municipio_value['_asentamientos']
			municipios.append(municipio_value)
		helpers.bulk(es, municipios, stats_only=True)
		del estado_value['_municipios']
		estados.append(estado_value)
	helpers.bulk(es, estados, stats_only=True)



def convert_file_to_json():
	cp_json = open('cp_json.json','w')
	data_dict = get_data_dict()
	cp_json.write(json.dumps(data_dict,encoding="utf-8"))

if __name__=='__main__':
	save(get_data_dict())
	# get_data_dict()
